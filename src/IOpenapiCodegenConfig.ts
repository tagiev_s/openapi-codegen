import { ICodegenOption } from "./models/ICodegenOption";

export interface IOpenapiCodegenConfig {
  path: string;
  rejectUnauthorized?: boolean;
  openApiVersion: "v2" | "v3";
  modelOption?: ICodegenOption;
  requestModelOption?: ICodegenOption;
}