import OpenapiProvider from "./providers/OpenapiProvider";
import SwaggerProvider from "./providers/SwaggerProvider";
import ModelCodegen from "./codegen/ModelCodegen";
import { ICodegenOption } from "./models/ICodegenOption";
import { IOpenapiProvider } from "./providers/IOpenapiProvider";
import { IOpenapiCodegenConfig } from "./IOpenapiCodegenConfig";
import { LockFileProvider } from "./LockFileProvider";
import { getOpenapiObjMap } from "./helpers/OpenapiHelper";

export class OpenapiCodegenWebpackPlugin {
  private lockFileProvider: LockFileProvider;

  constructor(
    private option: { config: IOpenapiCodegenConfig[], lockFilePath?: string }
  ) {
    this.lockFileProvider = new LockFileProvider(option);
  }

  public apply(compiler: any) {
    compiler.hooks.beforeCompile.tapPromise('OpenapiCodegenWebpackPlugin', (compilation: any) => {
      return this.generate();
    });
  }

  public async generate() {
    let provider: IOpenapiProvider;

    const openapiObjMap = await getOpenapiObjMap(this.option.config);
    const changed = await this.lockFileProvider.getChangedConfig(openapiObjMap);

    for (const item of changed) {
      if (item.openApiVersion === "v2") {
        provider = await SwaggerProvider.getInstance(openapiObjMap[item.path]);
      } else {
        provider = OpenapiProvider.getInstance(openapiObjMap[item.path]);
      }

      if (item.modelOption) {
        const apimodel = await provider.getResponseModel();
        ModelCodegen.createModelFile(apimodel, item.modelOption);
      }

      if (item.requestModelOption) {
        const apimodel = await provider.getRequestModel();
        ModelCodegen.createModelFile(apimodel, item.requestModelOption);
      }
    }

    await this.lockFileProvider.createLockFileIfNeeded();

  }
}