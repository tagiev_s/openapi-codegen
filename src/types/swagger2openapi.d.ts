declare module 'swagger2openapi' {
  export function convertObj(obj: object, options: any, callback: (options: any, err: any) => any): void;
}