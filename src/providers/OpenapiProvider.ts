import SwaggerParser from "swagger-parser";
import { IOpenapiProvider } from "./IOpenapiProvider";
import IApiModel from "../models/IApiModel";
import { BaseProvider } from "./BaseProvider";
import { OpenAPIV3, OpenAPI } from "openapi-types";

export default class OpenapiProvider extends BaseProvider implements IOpenapiProvider {
  constructor(
    swagger: OpenAPIV3.Document
  ) {
    super(swagger);
  }

  public static getInstance(openapiObj: OpenAPIV3.Document): IOpenapiProvider {
    return new OpenapiProvider(openapiObj);
  }

}