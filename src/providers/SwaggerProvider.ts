/// <reference path="../types/swagger2openapi.d.ts" />
import converter from "swagger2openapi";
import { IOpenapiProvider } from "./IOpenapiProvider";
import { BaseProvider } from "./BaseProvider";
import { OpenAPIV3 } from "openapi-types";

export default class SwaggerProvider extends BaseProvider implements IOpenapiProvider {
  constructor(
    swagger: OpenAPIV3.Document
  ) {
    super(swagger);
  }

  public static getInstance(openapiObj: object): Promise<IOpenapiProvider> {
    const promise = new Promise<IOpenapiProvider>((resolve, reject) => {
      converter.convertObj(openapiObj, { warnOnly: true, patch: true }, (err, options) => {
        if(err){
          reject(err);
        }
        const provider = new SwaggerProvider(options.openapi);
        resolve(provider);
      })
    });

    return promise;
  }
}