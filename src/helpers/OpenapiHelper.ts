import axios from "axios";
import * as https from "https";

import { IOpenapiCodegenConfig } from '../IOpenapiCodegenConfig';

export const getOpenapiObjMap = async (config: IOpenapiCodegenConfig[]) => {
  const map: {[key: string]: any} = {};
  for(const c of config) {
    const api = await httpsGetRequest(c.path, { rejectUnauthorized: c.rejectUnauthorized ?? true });
    map[c.path] = api;
  }
  
  return map;
}

// TODO: wait for axios 0.19.1 relaese
const httpsGetRequest = (url: string, params: https.RequestOptions) => {
  return new Promise<object>(function(resolve, reject) {
      var req = https.get(url, params, function(res) {
          if (res.statusCode && (res.statusCode < 200 || res.statusCode >= 300)) {
              return reject(new Error('statusCode=' + res.statusCode));
          }
          const body: any[] = [];
          res.on('data', function(chunk) {
              body.push(chunk);
          });
          res.on('end', function() {
              try {
                resolve(JSON.parse(Buffer.concat(body).toString()));
              } catch(e) {
                  reject(e);
              }
          });
      });
      req.on('error', function(err) {
          reject(err);
      });
      req.end();
  });
}