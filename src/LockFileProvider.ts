import { pathExists, readJSON, writeJSON } from "fs-extra";
import { IOpenapiCodegenConfig } from "./IOpenapiCodegenConfig";

interface ILockModel {
  url: string;
  apiHash: number | null;
  openApiVersion: string;
  modelOptionHash: number | null;
  requestModelOptionHash: number | null;
}

export class LockFileProvider {
  private lockFile: string;
  private lockValue: ILockModel[] = [];
  private isDirty = false;

  constructor(
    private option: { config: IOpenapiCodegenConfig[], lockFilePath?: string }
  ) {
    this.lockFile = option.lockFilePath || "codegen.lock";

  }

  public async getChangedConfig(openapiObjMap: {[path: string]: any}) {
    const lockExist = await pathExists(this.lockFile);
    const newConfig: IOpenapiCodegenConfig[] = [];
    this.lockValue = [];

    const lockValuePrev: ILockModel[] = lockExist ? await readJSON(this.lockFile) : [];

    for (const c of this.option.config) {
      const prev = lockValuePrev.find(x => x.url === c.path);

      const obj = openapiObjMap[c.path];
      const apiData: string = JSON.stringify(obj);
      const modelJson = JSON.stringify(c.modelOption);
      const requestModelJson = JSON.stringify(c.requestModelOption);

      const lockItem: ILockModel = {
        url: c.path,
        openApiVersion: c.openApiVersion,
        apiHash: this.hashCode(apiData),
        modelOptionHash: this.hashCode(modelJson),
        requestModelOptionHash: this.hashCode(requestModelJson)
      };
      this.lockValue.push(lockItem);

      if (!prev || prev.openApiVersion !== c.openApiVersion) {
        newConfig.push(c);
        this.isDirty = true;
      } else {
        if (lockItem.apiHash !== prev.apiHash) {
          newConfig.push(c);
          this.isDirty = true;
        }
        else {
          const newItem = { ...c };
          if (lockItem.modelOptionHash === prev.modelOptionHash) {
            newItem.modelOption = undefined;
          }
          if (lockItem.requestModelOptionHash === prev.requestModelOptionHash) {
            newItem.requestModelOption = undefined;
          }
          if (newItem.modelOption || newItem.requestModelOption) {
            newConfig.push(newItem);
            this.isDirty = true;
          }
        }
      }
    }
    if (lockExist) {
      return newConfig;
    } else {
      this.isDirty = true;
      return this.option.config;
    }
  }

  public async createLockFileIfNeeded() {
    if (this.isDirty) {
      await writeJSON(this.lockFile, this.lockValue);
    }
  }

  private hashCode = function (s: string) {
    return s ? s.split("").reduce(function (a, b) { a = ((a << 5) - a) + b.charCodeAt(0); return a & a }, 0) : null;
  }

}