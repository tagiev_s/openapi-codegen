interface IImportDecription {
  moduleSpecifier: string;
  namedImports?: [
    { name: string }
  ],
  defaultImport?: string
}

export interface ITypeReplacer {
  [sourceType: string]: { destType: string, importFrom?: IImportDecription }
}

export interface ICodegenOption {
  outDir: string;
  customTypeMapper: ITypeReplacer;
  fileCamelCase: boolean;
  cleanOutDir?: boolean;
}